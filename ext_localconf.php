<?php
defined('TYPO3') or die();

call_user_func(
    function($extKey)
    {
    	$extensionConfiguration = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Configuration\ExtensionConfiguration::class)->get($extKey);
		// Example of configuration of hooks
		// $GLOBALS['TYPO3_CONF_VARS']['EXTENSIONS'][$extKey]['tx_srfeuserregister_pi1']['confirmRegistrationClass'][] = 'SJBR\\SrFeuserRegister\\Hooks\\Handler';
		if (
			!isset($extensionConfiguration['tx_srfeuserregister_pi1']['registrationProcess'])
			|| !is_array($extensionConfiguration['tx_srfeuserregister_pi1']['registrationProcess'])
		) {
			$GLOBALS['TYPO3_CONF_VARS']['EXTENSIONS']['tx_srfeuserregister_pi1']['registrationProcess'] = [];
		}
        $registrationProcessHooks = $GLOBALS['TYPO3_CONF_VARS']['EXTENSIONS']['tx_srfeuserregister_pi1']['registrationProcess'] ?? [];
		if (!in_array(\SJBR\SrFeuserRegister\Hooks\RegistrationProcessHooks::class, $registrationProcessHooks)) {
		    $GLOBALS['TYPO3_CONF_VARS']['EXTENSIONS'][$extKey]['tx_srfeuserregister_pi1']['registrationProcess'][] = \SJBR\SrFeuserRegister\Hooks\RegistrationProcessHooks::class;
		}
		// Configure captcha hooks
		if (
			!isset($extensionConfiguration['captcha'])
			|| !is_array($extensionConfiguration['captcha'])
		) {
			$GLOBALS['TYPO3_CONF_VARS']['EXTENSIONS'][$extKey]['captcha'] = [];
		}
        $captchaHooks = $GLOBALS['TYPO3_CONF_VARS']['EXTENSIONS'][$extKey]['captcha'] ?? [];
		if (!in_array(\SJBR\SrFeuserRegister\Captcha\Freecap::class, $captchaHooks)) {
		    $GLOBALS['TYPO3_CONF_VARS']['EXTENSIONS'][$extKey]['captcha'][] = \SJBR\SrFeuserRegister\Captcha\Freecap::class;
		}
		// Configure usergroup hooks
		if (
			!isset($extensionConfiguration['tx_srfeuserregister_pi1']['configuration'])
			|| !is_array($extensionConfiguration['tx_srfeuserregister_pi1']['configuration'])
		) {
			$GLOBALS['TYPO3_CONF_VARS']['EXTENSIONS'][$extKey]['tx_srfeuserregister_pi1']['configuration'] = [];
		}
        $configurationHooks = $GLOBALS['TYPO3_CONF_VARS']['EXTENSIONS'][$extKey]['tx_srfeuserregister_pi1']['configuration'];
		if (!in_array(\SJBR\SrFeuserRegister\Hooks\UsergroupHooks::class, $configurationHooks)) {
		    $GLOBALS['TYPO3_CONF_VARS']['EXTENSIONS'][$extKey]['tx_srfeuserregister_pi1']['configuration'][] = \SJBR\SrFeuserRegister\Hooks\UsergroupHooks::class;
		}
		if (
			!isset($extensionConfiguration['tx_srfeuserregister_pi1']['fe_users']['usergroup'])
			|| !is_array($extensionConfiguration['tx_srfeuserregister_pi1']['fe_users']['usergroup'])
		) {
			$GLOBALS['TYPO3_CONF_VARS']['EXTENSIONS'][$extKey]['tx_srfeuserregister_pi1']['fe_users']['usergroup'] = [];
		}
        $userGroupHooks = $GLOBALS['TYPO3_CONF_VARS']['EXTENSIONS'][$extKey]['tx_srfeuserregister_pi1']['fe_users']['usergroup'];
		if (!in_array(\SJBR\SrFeuserRegister\Hooks\UsergroupHooks::class, $userGroupHooks)) {
		    $GLOBALS['TYPO3_CONF_VARS']['EXTENSIONS'][$extKey]['tx_srfeuserregister_pi1']['fe_users']['usergroup'][] = \SJBR\SrFeuserRegister\Hooks\UsergroupHooks::class;
		}
		// Configure file upload hooks
		if (
			!isset($extensionConfiguration['tx_srfeuserregister_pi1']['model'])
			|| !is_array($extensionConfiguration['tx_srfeuserregister_pi1']['model'])
		) {
			$GLOBALS['TYPO3_CONF_VARS']['EXTENSIONS'][$extKey]['tx_srfeuserregister_pi1']['model'] = [];
		}
        $fileUploadHooks = $GLOBALS['TYPO3_CONF_VARS']['EXTENSIONS'][$extKey]['tx_srfeuserregister_pi1']['model'];
		if (!in_array(\SJBR\SrFeuserRegister\Hooks\FileUploadHooks::class, $fileUploadHooks)) {
		    $GLOBALS['TYPO3_CONF_VARS']['EXTENSIONS'][$extKey]['tx_srfeuserregister_pi1']['model'][] = \SJBR\SrFeuserRegister\Hooks\FileUploadHooks::class;
		}
		// Register Status Report Hook
		$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['reports']['tx_reports']['status']['providers']['Front End User Registration'][] = \SJBR\SrFeuserRegister\Configuration\Reports\StatusProvider::class;
	},
	'sr_feuser_register'
);